# convolutional neural network 
# build with TensorFlow and trained on MNIST 

# import MNIST data
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('/tmp/data/',one_hot=False)

import tensorflow as tf

# parameters
learning_rate = 0.001
steps = 2000
batch_size = 128
inputs = 784 # MNIST shape : 28*28
classes = 10
dropout = 0.25

def conn_model_fn(features, labels, mode):
    # input layer
    input_layer = tf.reshape(features['x'], [-1,28,28,1]) # batch size(-1 for dynamic), width, height, channels

    # convolutional layer #1
    conv1 = tf.layers.conv2d
    (
        inputs = input_layer, # [batch_size,width,height,channels]
        filters = 32,
        kernel_size = [5,5],
        padding = 'same',
        activation = tf.nn.relu
    )   # output = [batch_size, 28, 28, 32]
    
    # pooling layer #1 
    pool1 = tf.layers.max_pooling2d(inputs=conv1,pool_size[2,2],strides=2) 
    # inputs = [batch_size, width, height, channels], outputs = [batch_size,14,14,32]
    
    # convolutional layer #2 and pooling layer #2
    conv2 = tf.layers.conv2d
    (
        inputs = pool1,
        filters=64, # dimension of output space
        kernel_size=[5,5],
        padding='same', # pad l/r evenly with 0
        activation=tf.nn.relu # applied to outputs
    )
    pool2 = tf.layers.max_pooling2d(inputs=conv2,pool_size=[2,2],strides=2)
    # inputs = [batch_size, 14, 14, 64], outputs = [batch_size, 7, 7, 64] 

    # dense layer
    pool2_flat = tf.reshape(pool2, [-1,7*7*64]) # [dynamic, width*height*channels]
    dense = tf.layers.dense(inputs=pool2_flat,units=1024,activation=tf.nn.relu)
    dropout = tf.layers.dropout(inputs=dense,rate=0.4,training=mode == tf.estimator.ModeKeys.TRAIN)
    # dropout only in training mode

    # logits layer
    logits = tf.layers.dense(inputs=dropout, units=10) # output [batch_size, 10]

    predictions = 
    {
            # generate predictions (for PREDICT and EVAL mode)
            'classes': tf.argmax(input=logits, axis=1), # [batch_size, 10]
            # add 'softmax_tensor' to the graph. For PREDICT,logging_hook 
            'probabilities': tf.nn.softmax(logits, name='softmax_tensor') # TODO
    }

    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)
    # calculate loss (for both TRAIN and EVAL modes)
    onehot_labels = tf.one_hot(indices=tf.cast(labels,tf.int32), depth=10) # idx of 1, num of target classes
    loss = tf.losses.sparse_softmax_cross_entropy(labels=labels,logits=logits)
    # configure Training Op (for TRAIN mode)
    if mode = tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.GradientsDescentOptimizer(learning_rate=0.001)
        train_op = optimizer.minimize
        (
            loss = loss,
            global_step=tf.train.get_global_step()
        )
    return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)
    # add evaluation metrics (for EVAL mode)
    eval_metric_ops = 
    {
        'accuracy': tf.metrics.accuracy(labels=labels,predictions=predictions['classes'])
    }
    return tf.estimator.EstimatorSpec(mode=mode,loss=loss,eval_metric_ops=eval_metric_ops)


